/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                            Programming Skills                             */
/*                                                                           */
/*---------------------------------------------------------------------------*/

//3 high flags, 2 low flags, 4 caps, and a center park
//(2 + 2 + 2)  +  (1 + 1)  +  (1 + 1 + 1 + 1)  +  6
//Total of 18 Points

void ProgrammingSkills(void)
{
    //Score Left Top Flag
    fire();

    //Intake ball from under cap
    rawmove(1600, -60);
    drive(25, 50);
    turn(90, 40);
    rawmove(750, 70);
    intake(100);
    drive(-40, 50);
    vex::sleepMs(500);
    drive(3, 20);
    intake(-100);
    vex::sleepMs(300);
    drive(-16, 60);
    vex::sleepMs(300);
    intake(-10);

    //score Left Middle Flag
    drive(50, 60);
    intake(10);
    rawmove(1100, 40);

    drive(-6, 20);
    turn(-93, 20);

    //Flip Left Bottom Flag and intake ball from under cap
    drive(80, 70);
    intake(30);
    rawmove(1500, 90);
    intake(10);
    drive(-21, 30);
    turn(-6, 10);
    vex::sleepMs(200);
    fire();

    vex::sleepMs(300);
    turn(10, 10);
    intake(-10);
    drive(-26, 30);

    turn(95, 40);
    rawmove(1000, 40);
    intake(100);
    drive(-44, 50);
    vex::sleepMs(500);
    intake(-100);
    vex::sleepMs(300);
    drive(-10, 60);
    vex::sleepMs(300);
    intake(-30);
    drive(-8, 30);

    //Score Center Top Flag
    turn(-90, 20);
    intake(0);
    rawmove(750, -40);

    drive(10, 25);
    intake(20);

    turn(32, 15);
    drive(17, 35);
    turn(-33, 20);
    rawmove(1500, 80);

    drive(-24, 25);
    vex::sleepMs(200);
    turn(-3, 10);
    vex::sleepMs(200);
    fire();

    vex::sleepMs(300);
    turn(5, 10);

    //Flip Center Bottom Flag and flip cap

    drive(-4, 25);
    turn(-90, 30);
    drive(40, 60);
    flip();
    drive(-10, 60);

    turn(184, 20);
    drive(45, 60);
    flip();

    //Center Park
    drive(-10, 60);
    turn(-90, 30);
    rawmove(2000, -40);
    drive(-28, 90);
    turn(90, 15);
    rawmove(500, -40);
    drive(-26, 90);
    intake(0);
}