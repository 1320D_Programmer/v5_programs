/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                            Autonomous Routines                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/

void Autonomous(void)
{
    AutonLCD();

    switch (autonomousSelection)
    {
    //Red Front Flags
    case 0:
        intake(100);
        drive(-36, 80);
        vex::sleepMs(300);
        rawmove(1000, 70);
        lineUp(150, 60);
        turn(-96, 30);
        intake(20);
        drive(14, 30);
        fire();
        intake(0);
        drive(9, 40);
        turn(-91, 40);
        intake(20);
        vex::sleepMs(100);
        drive(37, 70);
        flip();
        if (ParkChoice == 1)
        {
            turn(92, 40);
            intake(100);
            drive(-28, 90);
            rawmove(500, -70);
            drive(-26, 90);
        }
        break;
    //Red Back Caps
    case 1:
        intake(100);
        drive(-40, 80);
        vex::sleepMs(600);
        clear();
        intake(30);
        FL.rotateTo(480, vex::rotationUnits::deg, -25, vex::velocityUnits::pct, false);
        BL.rotateTo(480, vex::rotationUnits::deg, -25, vex::velocityUnits::pct, false);
        FR.rotateTo(1440, vex::rotationUnits::deg, -75, vex::velocityUnits::pct, false);
        BR.rotateTo(1440, vex::rotationUnits::deg, -75, vex::velocityUnits::pct, true);

        drive(11, 70);
        intake(10);

        Flipper1.rotateTo(250, vex::rotationUnits::deg, 100, vex::velocityUnits::pct, false);
        Flipper2.rotateTo(250, vex::rotationUnits::deg, 100, vex::velocityUnits::pct);
        Flipper1.rotateTo(0, vex::rotationUnits::deg, 100, vex::velocityUnits::pct, false);
        Flipper2.rotateTo(0, vex::rotationUnits::deg, 100, vex::velocityUnits::pct);

        if (ParkChoice == 1)
        {
            turn(-80, 50);
            rawmove(1500, -60);
            drive(-26, 90);
        }

        break;
    //Programming Skills
    case 2:
        ProgrammingSkills5();
        break;
    //Blue Front Flags
    case 3:

        intake(100);
        drive(-37, 60);
        vex::sleepMs(300);
        rawmove(1000, 50);
        lineUp(150, 40);
        turn(92, 25);
        intake(20);
        drive(12, 30);
        fire();
        intake(0);
        drive(11, 35);
        turn(90, 30);
        intake(20);
        vex::sleepMs(100);
        drive(32, 60);
        flip();
        if (ParkChoice == 1)
        {
            turn(-92, 40);
            intake(100);
            drive(-28, 90);
            rawmove(600, -60);
            drive(-27, 90);
        }
        break;
    //Blue Back Caps
    case 4:

        intake(100);
        drive(-42, 80);
        vex::sleepMs(600);
        clear();
        intake(30);
        FL.rotateTo(1440, vex::rotationUnits::deg, -75, vex::velocityUnits::pct, false);
        BL.rotateTo(1440, vex::rotationUnits::deg, -75, vex::velocityUnits::pct, false);
        FR.rotateTo(480, vex::rotationUnits::deg, -25, vex::velocityUnits::pct, false);
        BR.rotateTo(480, vex::rotationUnits::deg, -25, vex::velocityUnits::pct, true);

        drive(13, 70);
        intake(10);

        Flipper1.rotateTo(250, vex::rotationUnits::deg, 100, vex::velocityUnits::pct, false);
        Flipper2.rotateTo(250, vex::rotationUnits::deg, 100, vex::velocityUnits::pct);
        Flipper1.rotateTo(0, vex::rotationUnits::deg, 100, vex::velocityUnits::pct, false);
        Flipper2.rotateTo(0, vex::rotationUnits::deg, 100, vex::velocityUnits::pct);

        if (ParkChoice == 1)
        {
            turn(60, 50);
            rawmove(1500, -60);
            drive(-26, 90);
        }

        break;
    //Do Nothing
    default:

        break;
    }
}