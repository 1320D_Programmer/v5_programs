/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                           V5 Display Functions                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/

/*//////////////////////////*/
/*       Rainbow Logo       */
/*//////////////////////////*/
void DriverLCD()
{

    vex::color c;
    int hue = 0;

    while (Competition.isDriverControl() && Competition.isEnabled())
    {

        Brain.Screen.clearScreen();

        /*//////////////////////////*/
        /*    Battery Indication    */
        /*//////////////////////////*/

        Brain.Screen.setFont(vex::fontType::mono20);

        Brain.Screen.setPenWidth(22);
        Brain.Screen.setPenColor(vex::color::red);
        Brain.Screen.drawLine(237, 215, 459, 215);

        Brain.Screen.setPenColor(vex::color::green);
        Brain.Screen.drawLine(237, 215, 347 + (2.2 * (Brain.Battery.capacity(vex::percentUnits::pct)) - 110), 215);

        Brain.Screen.setFillColor(vex::color::transparent);
        Brain.Screen.setPenWidth(2);
        Brain.Screen.setPenColor(vex::color::white);

        Brain.Screen.drawRectangle(236, 203, 222, 22);

        Brain.Screen.setFillColor(vex::color::white);

        Brain.Screen.setPenWidth(10);
        Brain.Screen.drawLine(460, 215, 463, 215);

        Brain.Screen.setPenColor(vex::color::black);

        if (Brain.Battery.capacity(vex::percentUnits::pct) == 100)
        {
            Brain.Screen.setFillColor(vex::color::green);
            Brain.Screen.printAt((2.2 * (Brain.Battery.capacity(vex::percentUnits::pct)) + 205), 221, "%d", Brain.Battery.capacity(vex::percentUnits::pct));
        }
        else if (Brain.Battery.capacity(vex::percentUnits::pct) > 6)
        {
            Brain.Screen.setFillColor(vex::color::green);
            Brain.Screen.printAt((2.2 * (Brain.Battery.capacity(vex::percentUnits::pct)) + 215), 221, "%d", Brain.Battery.capacity(vex::percentUnits::pct));
        }
        else
        {
            Brain.Screen.setFillColor(vex::color::red);
            Brain.Screen.printAt((2.2 * (Brain.Battery.capacity(vex::percentUnits::pct)) + 240), 221, "%d", Brain.Battery.capacity(vex::percentUnits::pct));
        }

        /*//////////////////////////*/
        /*      Glowing D Logo      */
        /*//////////////////////////*/
        Brain.Screen.setPenWidth(10);
        Brain.Screen.setFillColor(vex::color::black);

        Brain.Screen.setPenColor(vex::color::white);
        Brain.Screen.drawCircle(115, 120, 70, vex::color::transparent);

        Brain.Screen.setPenColor(vex::color::black);
        Brain.Screen.drawRectangle(45, 50, 70, 140);

        Brain.Screen.setPenColor(vex::color::white);
        Brain.Screen.drawLine(100, 52, 124, 52);

        Brain.Screen.setPenColor(vex::color::white);
        Brain.Screen.drawLine(100, 190, 124, 190);

        // set color, use hue, saturation, value
        c.hsv(hue, 1.0, 1.0);
        // increase hue, it's in range 0 to 360 degrees
        hue = (hue + 5) % 360;
        // set drawing color
        Brain.Screen.setPenColor(c);
        Brain.Screen.setPenWidth(5);

        Brain.Screen.drawCircle(115, 120, 105, vex::color::transparent);

        Brain.Screen.setPenWidth(10);

        Brain.Screen.drawLine(85, 46, 85, 195);

        /*//////////////////////////*/
        /*       Display Data       */
        /*//////////////////////////*/

        Brain.Screen.setFont(vex::fontType::mono20);

        if (AutoCatapult)
        {
            Brain.Screen.printAt(380, 70, "snazzy");
        }
        else
        {
            Brain.Screen.printAt(380, 70, "not snazzy");
        }

        if (AutoIntake)
        {
            Brain.Screen.printAt(360, 90, "snazzy");
        }
        else
        {
            Brain.Screen.printAt(360, 90, "not snazzy");
        }

        if (CatLimit.pressing())
        {
            Brain.Screen.printAt(340, 130, "snazzy");
        }
        else
        {
            Brain.Screen.printAt(340, 130, "not snazzy");
        }

        Brain.Screen.setFont(vex::fontType::mono60);

        Brain.Screen.printAt(352, 45, "D");

        Brain.Screen.setPenColor(vex::color::white);

        Brain.Screen.printAt(230, 45, "1320");

        Brain.Screen.setFont(vex::fontType::mono20);

        Brain.Screen.printAt(237, 70, "AutoCatapult: ");
        Brain.Screen.printAt(237, 90, "AutoIntake: ");
        Brain.Screen.printAt(237, 130, "CatLimit: ");
        Brain.Screen.printAt(237, 150, "Heading: %1.0f degrees", (LeftQuad.rotation(vex::rotationUnits::deg) - RightQuad.rotation(vex::rotationUnits::deg)) / 3.6);

        Brain.Screen.printAt(237, 170, "FL: %1.0f degrees", LeftQuad.rotation(vex::rotationUnits::deg));
        Brain.Screen.printAt(237, 190, "FR: %1.0f degrees", RightQuad.rotation(vex::rotationUnits::deg));

        //Brain.Screen.printAt(237, 170, "Left: %1.0f mm", LeftSonar.distance(vex::distanceUnits::mm));
        //Brain.Screen.printAt(237, 190, "Right: %1.0f mm", RightSonar.distance(vex::distanceUnits::mm));

        // stops flicker at expense of lower update rate
        //Brain.Screen.render();
        vex::sleepMs(250);
    }
}
