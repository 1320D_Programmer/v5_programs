void drivePID(int distance, int velocity)
{

    clear();
    SetTorque(100);

    int output = velocity;
    int increment = velocity / 20;
    int currentSpeed = increment * (distance / abs(distance));
    double target = distance * inchConvert;

    if (distance > 0)
    {
        for (int rampUp = 0; rampUp <= 20; rampUp++)
        {
            currentSpeed += increment;
            FL.rotateTo(target, vex::rotationUnits::deg, -currentSpeed, vex::velocityUnits::pct, false);
            BL.rotateTo(target, vex::rotationUnits::deg, -currentSpeed, vex::velocityUnits::pct, false);
            FR.rotateTo(target, vex::rotationUnits::deg, -currentSpeed, vex::velocityUnits::pct, false);
            BR.rotateTo(target, vex::rotationUnits::deg, -currentSpeed, vex::velocityUnits::pct, false);
            vex::sleepMs(10);
        }

        while (FL.rotation(vex::rotationUnits::deg) < target - 360 || FR.rotation(vex::rotationUnits::deg) < target - 360)
        {
            FL.rotateTo(target, vex::rotationUnits::deg, -velocity, vex::velocityUnits::pct, false);
            BL.rotateTo(target, vex::rotationUnits::deg, -velocity, vex::velocityUnits::pct, false);
            FR.rotateTo(target, vex::rotationUnits::deg, -velocity, vex::velocityUnits::pct, false);
            BR.rotateTo(target, vex::rotationUnits::deg, -velocity, vex::velocityUnits::pct, false);
            vex::sleepMs(10);
        }

        while (FL.rotation(vex::rotationUnits::deg) < target - 10 || FR.rotation(vex::rotationUnits::deg) < target - 10)
        {
            output = output * 0.8;

            FL.rotateTo(target, vex::rotationUnits::deg, -output, vex::velocityUnits::pct, false);
            BL.rotateTo(target, vex::rotationUnits::deg, -output, vex::velocityUnits::pct, false);
            FR.rotateTo(target, vex::rotationUnits::deg, -output, vex::velocityUnits::pct, false);
            BR.rotateTo(target, vex::rotationUnits::deg, -output, vex::velocityUnits::pct, false);
            vex::sleepMs(10);
        }
    }
    else
    {
        for (int rampUp = 0; rampUp <= 30; rampUp++)
        {
            currentSpeed += increment;
            FL.rotateTo(target, vex::rotationUnits::deg, -currentSpeed, vex::velocityUnits::pct, false);
            BL.rotateTo(target, vex::rotationUnits::deg, -currentSpeed, vex::velocityUnits::pct, false);
            FR.rotateTo(target, vex::rotationUnits::deg, -currentSpeed, vex::velocityUnits::pct, false);
            BR.rotateTo(target, vex::rotationUnits::deg, -currentSpeed, vex::velocityUnits::pct, false);
            vex::sleepMs(10);
        }

        while (FL.rotation(vex::rotationUnits::deg) > target + 180 || FR.rotation(vex::rotationUnits::deg) > target + 360)
        {
            FL.rotateTo(target, vex::rotationUnits::deg, -velocity, vex::velocityUnits::pct, false);
            BL.rotateTo(target, vex::rotationUnits::deg, -velocity, vex::velocityUnits::pct, false);
            FR.rotateTo(target, vex::rotationUnits::deg, -velocity, vex::velocityUnits::pct, false);
            BR.rotateTo(target, vex::rotationUnits::deg, -velocity, vex::velocityUnits::pct, false);
            vex::sleepMs(10);
        }

        while (FL.rotation(vex::rotationUnits::deg) > target + 10 || FR.rotation(vex::rotationUnits::deg) > target + 10)
        {
            output = output * 0.8;

            FL.rotateTo(target, vex::rotationUnits::deg, -output, vex::velocityUnits::pct, false);
            BL.rotateTo(target, vex::rotationUnits::deg, -output, vex::velocityUnits::pct, false);
            FR.rotateTo(target, vex::rotationUnits::deg, -output, vex::velocityUnits::pct, false);
            BR.rotateTo(target, vex::rotationUnits::deg, -output, vex::velocityUnits::pct, false);
            vex::sleepMs(10);
        }
    }
}