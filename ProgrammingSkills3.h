
void ProgrammingSkills3(void)
{

    intake(100);
    drive(-43, 70);
    vex::sleepMs(300);

    clear();
    FL.rotateTo(460, vex::rotationUnits::deg, -25, vex::velocityUnits::pct, false);
    BL.rotateTo(460, vex::rotationUnits::deg, -25, vex::velocityUnits::pct, false);
    FR.rotateTo(1380, vex::rotationUnits::deg, -75, vex::velocityUnits::pct, false);
    BR.rotateTo(1380, vex::rotationUnits::deg, -75, vex::velocityUnits::pct, true);

    drive(25, 70);
    intake(10);

    Flipper1.rotateTo(250, vex::rotationUnits::deg, 100, vex::velocityUnits::pct, false);
    Flipper2.rotateTo(250, vex::rotationUnits::deg, 100, vex::velocityUnits::pct);
    Flipper1.rotateTo(0, vex::rotationUnits::deg, 100, vex::velocityUnits::pct, false);
    Flipper2.rotateTo(0, vex::rotationUnits::deg, 100, vex::velocityUnits::pct);

    turn(83, 40);
    drive(13, 40);
    flip();
    intake(0);
    rawmove(1000, -80);

    drive(4, 50);
    intake(30);
    turn(55, 40);
    drive(46, 90);
    vex::sleepMs(100);
    turn(-65, 40);
    vex::sleepMs(100);
    drive(55, 90);
    turn(100, 60);
    rawmove(750, 70);
    vex::sleepMs(250);
    drive(-6, 20);
    turn(-94, 30);
    rawmove(1500, 90);
    vex::sleepMs(250);

    drive(-6, 100);
    //////////////////////////////////////
    turn(-15, 20);
    rawmove(1000, 40);
    vex::sleepMs(200);
    drive(-35, 50);

    turn(-4, 10);

    fire();

    intake(20);

    drive(-20, 60);
    vex::sleepMs(100);
    turn(100, 50);
    rawmove(1000, 60);
    vex::sleepMs(250);
    intake(100);
    drive(-43, 80);
    vex::sleepMs(400);
    intake(30);
    drive(4, 40);
    turn(-190, 50);
    drive(7, 40);

    Flipper1.rotateTo(250, vex::rotationUnits::deg, 70, vex::velocityUnits::pct, false);
    Flipper2.rotateTo(250, vex::rotationUnits::deg, 70, vex::velocityUnits::pct);
    Flipper1.rotateTo(0, vex::rotationUnits::deg, 100, vex::velocityUnits::pct, false);
    Flipper2.rotateTo(0, vex::rotationUnits::deg, 100, vex::velocityUnits::pct);

    turn(75, 50);
    intake(0);
    rawmove(800, -50);
    vex::sleepMs(150);
    drive(17, 40);
    clear();
    FL.rotateTo(100, vex::rotationUnits::deg, -20, vex::velocityUnits::pct, false);
    BL.rotateTo(100, vex::rotationUnits::deg, -20, vex::velocityUnits::pct, true);
    fire();

    drive(28, 70);
    turn(25, 50);
    rawmove(1000, 80);

    drive(-26, 50);
    turn(95, 40);
    drive(25, 70);
    flip();

    turn(174, 40);
    drive(65, 80);
    turn(-12, 30);

    Flipper1.rotateTo(230, vex::rotationUnits::deg, 70, vex::velocityUnits::pct, false);
    Flipper2.rotateTo(230, vex::rotationUnits::deg, 70, vex::velocityUnits::pct);
    Flipper1.rotateTo(0, vex::rotationUnits::deg, 100, vex::velocityUnits::pct, false);
    Flipper2.rotateTo(0, vex::rotationUnits::deg, 100, vex::velocityUnits::pct);

    //Ram against right bottom flag - 9pts
    turn(97, 45);
    rawmove(1500, 80);

    //Back up and get ball from under cap
    drive(-55, 80);
    intake(100);
    turn(-110, 50);
    drive(-17, 70);
    vex::sleepMs(400);
    intake(30);
    drive(5, 40);

    //Align against platform to turn and shoot right top flag - 11pts
    turn(90, 70);
    intake(0);
    rawmove(750, -60);
    vex::sleepMs(100);
    drive(19, 45);
    clear();
    FL.rotateTo(140, vex::rotationUnits::deg, -30, vex::velocityUnits::pct, false);
    BL.rotateTo(140, vex::rotationUnits::deg, -30, vex::velocityUnits::pct, true);
    fire();

    //Center Park - 17pts
    intake(100);
    turn(30, 50);
    rawmove(1200, -60);
    drive(-26, 90);
    vex::sleepMs(100);
    turn(-115, 50);
    vex::sleepMs(100);
    rawmove(500, -40);
    drive(-27, 90);
    intake(0);
}