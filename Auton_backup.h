/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                            Autonomous Routines                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/

void Autonomous(void)
{

    switch (autonomousSelection)
    {
    //Red Front Flags
    case 0:

        while (!CatLimit.pressing())
            vex::sleepMs(10);
        vex::sleepMs(300);
        shoot = true;
        while (shoot == true)
            vex::sleepMs(10);
        vex::sleepMs(200);
        turn(-5, 10);
        rawmove(1100, 100);
        rawmove(750, 70);
        drive(-27, 70);
        turn(-93, 30);
        rawmove(700, -50);
        drive(15, 70);
        flip();
        rawmove(900, -60);
        if (ParkChoice == 0)
        {
            drive(10, 60);
            turn(-90, 30);
            drive(48, 70);
            turn(85, 30);
            rawmove(1000, 50);
            movetime(1100, 100, 2);
        }

        break;
    //Red Back Caps
    case 1:

        break;
    //Programming Skills
    case 2:
        ProgrammingSkills();
        break;
    //Blue Front Flags
    case 3:

        while (!CatLimit.pressing())
            vex::sleepMs(10);
        vex::sleepMs(250);
        shoot = true;
        while (shoot == true)
            vex::sleepMs(10);
        vex::sleepMs(200);
        turn(-10, 10);
        rawmove(1100, 100);
        rawmove(750, 60);
        drive(-23, 70);
        turn(93, 30);
        rawmove(800, -40);
        drive(15, 60);
        flip();
        rawmove(900, -50);
        if (ParkChoice == 0)
        {
            drive(10, 50);
            turn(90, 30);
            drive(45, 70);
            turn(-92, 30);
            rawmove(1100, 40);
            movetime(1100, 100, 2);
        }
        break;
    //Blue Back Caps
    case 4:

        break;
    //Do Nothing
    default:

        turn(3600, 30);

        break;
    }
}
