/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                            Autonomous Routines                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/

void Autonomous(void)
{
    AutonLCD();

    switch (autonomousSelection)
    {
    //Red Front Flags
    case 0:

        while (!CatLimit.pressing())
            vex::sleepMs(10);
        vex::sleepMs(250);
        fire();
        drive(-4, 30);
        turn(90, 30);

        intake(100);
        drive(-35, 40);
        vex::sleepMs(500);
        intake(20);
        drive(45, 60);
        rawmove(500, 60);
        drive(-6, 30);
        turn(-94, 15);
        rawmove(1500, 90);
        intake(10);
        drive(-22, 25);
        turn(-10, 10);
        vex::sleepMs(200);
        fire();

        break;
    //Red Back Caps
    case 1:
        drive(10, 20);
        turn(-33, 10);
        drive(-10, 20);
        fire();
        turn(105, 40);
        rawmove(500, 70);
        intake(100);
        drive(-48, 60);
        vex::sleepMs(400);
        drive(12, 40);
        intake(0);
        turn(120, 30);
        drive(12, 30);
        flip();

        if (ParkChoice == 1)
        {
            drive(-13, 30);
            turn(-30, 30);
            rawmove(750, -40);
            drive(-25, 100);
        }
        break;
    //Programming Skills
    case 2:
        ProgrammingSkills3();
        break;
    //Blue Front Flags
    case 3:

        while (!CatLimit.pressing())
            vex::sleepMs(10);
        vex::sleepMs(250);
        fire();
        drive(-4, 30);
        turn(-100, 30);

        intake(100);
        drive(-35, 40);
        vex::sleepMs(500);
        intake(20);
        drive(45, 60);
        rawmove(500, 60);
        drive(-6, 30);
        turn(82, 15);
        rawmove(1500, 90);
        intake(10);
        drive(-20, 30);
        turn(7, 10);
        vex::sleepMs(200);
        fire();

        break;
    //Blue Back Caps
    case 4:

        drive(10, 20);
        turn(25, 10);
        drive(-10, 20);
        fire();
        turn(-105, 30);
        rawmove(750, 60);
        intake(100);
        drive(-48, 60);
        vex::sleepMs(400);
        drive(12, 40);
        intake(0);
        turn(-125, 30);
        drive(14, 30);
        flip();

        if (ParkChoice == 1)
        {
            drive(-10, 30);
            turn(30, 30);
            rawmove(750, -40);
            drive(-27, 100);
        }

        break;
    //Do Nothing
    default:

        break;
    }
}