
void ProgrammingSkills5(void)
{

    intake(100);
    drive(-42, 80);
    vex::sleepMs(600);
    intake(-60);
    drive(-12, 50);
    clear();
    intake(0);
    FL.rotateTo(480, vex::rotationUnits::deg, -25, vex::velocityUnits::pct, false);
    BL.rotateTo(480, vex::rotationUnits::deg, -25, vex::velocityUnits::pct, false);
    FR.rotateTo(1440, vex::rotationUnits::deg, -75, vex::velocityUnits::pct, false);
    BR.rotateTo(1440, vex::rotationUnits::deg, -75, vex::velocityUnits::pct, true);

    drive(8, 70);
    intake(10);

    Flipper1.rotateTo(250, vex::rotationUnits::deg, 100, vex::velocityUnits::pct, false);
    Flipper2.rotateTo(250, vex::rotationUnits::deg, 100, vex::velocityUnits::pct);
    Flipper1.rotateTo(0, vex::rotationUnits::deg, 100, vex::velocityUnits::pct, false);
    Flipper2.rotateTo(0, vex::rotationUnits::deg, 100, vex::velocityUnits::pct);

    intake(30);
    turn(-35, 40);
    drive(-45, 90);

    clear();
    FR.rotateTo(-350, vex::rotationUnits::deg, 80, vex::velocityUnits::pct, false);
    BR.rotateTo(-350, vex::rotationUnits::deg, 80, vex::velocityUnits::pct, true);

    intake(-100);
    clear();
    FL.rotateTo(-1200, vex::rotationUnits::deg, 80, vex::velocityUnits::pct, false);
    BL.rotateTo(-1200, vex::rotationUnits::deg, 80, vex::velocityUnits::pct, false);
    FR.rotateTo(-1600, vex::rotationUnits::deg, 80, vex::velocityUnits::pct, false);
    BR.rotateTo(-1600, vex::rotationUnits::deg, 80, vex::velocityUnits::pct, true);
    intake(30);

    // vex::sleepMs(100);
    // turn(-70, 40);
    // vex::sleepMs(100);
    // drive(-55, 90);
    // turn(-125, 60);
    rawmove(1400, 70);
    vex::sleepMs(250);
    drive(-5, 20);
    turn(-93, 30);
    rawmove(1200, 90);
    vex::sleepMs(250);

    drive(-6, 90);
    turn(-15, 20);
    rawmove(1000, 70);
    vex::sleepMs(100);

    drive(-35, 50);

    turn(-0.5, 10);
    vex::sleepMs(100);
    fire();

    intake(50);

    drive(-17, 60);
    vex::sleepMs(100);
    turn(110, 50);
    lineUp(150, 40);
    //rawmove(1000, 60);
    vex::sleepMs(100);
    intake(100);
    drive(-40, 80);
    //drive(-42, 80);
    vex::sleepMs(500);

    intake(30);
    lineUp(1050, 30);
    turn(-90, 50);
    intake(10);
    rawmove(1000, -50);
    vex::sleepMs(150);
    drive(10, 40);

    turn(-18, 40);

    rawmove(2000, 80);

    drive(-6, 90);
    turn(-15, 20);
    rawmove(1000, 60);
    vex::sleepMs(100);

    drive(-35, 50);

    turn(-3, 10);
    vex::sleepMs(100);
    fire();
    intake(50);

    clear();
    FL.rotateTo(500, vex::rotationUnits::deg, -70, vex::velocityUnits::pct, false);
    BL.rotateTo(500, vex::rotationUnits::deg, -70, vex::velocityUnits::pct, true);

    drive(41, 70);

    Flipper1.rotateTo(230, vex::rotationUnits::deg, 70, vex::velocityUnits::pct, false);
    Flipper2.rotateTo(230, vex::rotationUnits::deg, 70, vex::velocityUnits::pct);
    Flipper1.rotateTo(0, vex::rotationUnits::deg, 100, vex::velocityUnits::pct, false);
    Flipper2.rotateTo(0, vex::rotationUnits::deg, 100, vex::velocityUnits::pct);

    //Ram against right bottom flag - 9pts
    turn(87, 45);
    rawmove(1500, 80);
    vex::sleepMs(100);

    //Back up and get ball from under cap
    drive(-51, 80);
    intake(100);
    turn(-115, 40);
    //drive(-15, 70);
    //vex::sleepMs(500);
    lineUp(1100, 80);
    intake(-60);
    drive(-13, 40);
    lineUp(975, 30);

    intake(30);

    //Align against platform to turn and shoot right top flag - 11pts
    turn(90, 70);
    intake(30);
    rawmove(750, -60);
    vex::sleepMs(100);
    drive(25, 45);
    clear();
    FL.rotateTo(170, vex::rotationUnits::deg, -30, vex::velocityUnits::pct, false);
    BL.rotateTo(170, vex::rotationUnits::deg, -30, vex::velocityUnits::pct, true);
    fire();

    //Center Park - 17pts
    intake(100);
    turn(35, 50);
    rawmove(1400, -60);
    drive(-27, 90);
    vex::sleepMs(150);
    turn(-125, 45);
    vex::sleepMs(100);
    rawmove(500, -40);
    drive(-27, 90);
    intake(0);
}