/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                      Vision Sensor Automatic Aiming                       */
/*                                                                           */
/*---------------------------------------------------------------------------*/

void AutoDistance(int ideal_size)
{
    //#region config_init
    VisionSensor.setBrightness(50);
    VisionSensor.setSignature(sig_RED_FLAG);
    //#endregion config_init

    bool distance = false;
    int AverageWidth;

    while (distance == false)
    {
        for (for (int count = 0; count < 5; count++))
        {
        }

        //snap a picture
        VisionSensor.takeSnapshot(sig_RED_FLAG);
        //did we see anything?
        if (VisionSensor.objectCount > 0)
        {
            //where was the largest thing?
            if (VisionSensor.largestObject.width < ideal_size - 5)
            {
                //on the left, turn left
                move(10);
            }
            else if (VisionSensor.largestObject.width > ideal_size + 5)
            {
                //on the right, turn right
                move(-10);
            }
            else
            {
                //in the middle, we're done lining up
                linedup = true;
                cease(1);
            }
        }
        else
        {
            //saw nothing, relax
            cease(1);
        }
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void AutoCenter(void)
{
    //#region config_init
    VisionSensor.setBrightness(50);
    VisionSensor.setSignature(sig_RED_FLAG);
    //#endregion config_init

    //camera image is 316 pixels wide, so the center is 316/2
    int screen_middle_x = 316 / 2;
    bool linedup = false;

    while (linedup == false)
    {
        //snap a picture
        VisionSensor.takeSnapshot(sig_RED_FLAG);
        //did we see anything?
        if (VisionSensor.objectCount > 0)
        {
            //where was the largest thing?
            if (VisionSensor.largestObject.centerX < screen_middle_x - 5)
            {
                //on the left, turn left
                turnright(7);
            }
            else if (VisionSensor.largestObject.centerX > screen_middle_x + 5)
            {
                //on the right, turn right
                turnleft(7);
            }
            else
            {
                //in the middle, we're done lining up
                linedup = true;
                cease(1);
            }
        }
        else
        {
            //saw nothing, relax
            cease(1);
        }
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int screen_origin_x = 150;
int screen_origin_y = 20;
int screen_width = 316;
int screen_height = 212;

// function to draw a single object
void drawObject(vision::object &obj)
{
    int labelOffset = 0;

    Brain.Screen.setPenColor(vex::color::yellow);
    Brain.Screen.drawRectangle(screen_origin_x + obj.originX, screen_origin_y + obj.originY, obj.width, obj.height);
    Brain.Screen.setFont(vex::fontType::mono12);

    if (obj.originX > 280)
        labelOffset = -40;
    if (obj.originY > 10)
        Brain.Screen.printAt(screen_origin_x + obj.originX + labelOffset, screen_origin_y + obj.originY - 3, "Sig %o", obj.id);
    else
        Brain.Screen.printAt(screen_origin_x + obj.originX + labelOffset, screen_origin_y + obj.originY + 10, "Sig %o", obj.id);
}

// function to draw all objects found
void drawObjects(vision &v)
{
    Brain.Screen.setPenColor(vex::color::black);
    Brain.Screen.drawRectangle(screen_origin_x, screen_origin_y, screen_width, screen_height, vex::color::black);

    for (int i = 0; i < v.objectCount; i++)
        drawObject(v.objects[i]);
}

void VisionDisplay(void)
{
    // Draw an area representing the vision sensor field of view
    Brain.Screen.clearScreen(vex::color::black);
    Brain.Screen.setPenColor(vex::color::green);
    Brain.Screen.drawRectangle(screen_origin_x - 1, screen_origin_y - 1, screen_width + 2, screen_height + 2);

    while (1)
    {
        // request any objects with signature 1
        int n = VisionSensor.takeSnapshot(sig_RED_FLAG);

        Brain.Screen.setPenColor(vex::color::white);
        Brain.Screen.setFont(mono20);
        Brain.Screen.setCursor(2, 2);
        Brain.Screen.print("objects: %2d", (int)n);

        Brain.Screen.setFont(mono15);

        Brain.Screen.setCursor(6, 2);
        Brain.Screen.print("Sig:      %3d", VisionSensor.largestObject.id);
        Brain.Screen.setCursor(7, 2);
        Brain.Screen.print("Center X: %3d", VisionSensor.largestObject.centerX);
        Brain.Screen.setCursor(8, 2);
        Brain.Screen.print("Center Y: %3d", VisionSensor.largestObject.centerY);
        Brain.Screen.setCursor(9, 2);
        Brain.Screen.print("Width:    %3d", VisionSensor.largestObject.width);
        Brain.Screen.setCursor(10, 2);
        Brain.Screen.print("Height:   %3d", VisionSensor.largestObject.height);
        Brain.Screen.setCursor(11, 2);

        // draw any objects found
        drawObjects(VisionSensor);

        // run 10 times/second
        vex::sleepMs(100);
    }
}