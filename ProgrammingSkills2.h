/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                            Programming Skills                             */
/*                                                                           */
/*---------------------------------------------------------------------------*/

//3 high flags, 3 low flags, 2 caps, and a center park
//(2 + 2 + 2)  +  (1 + 1 + 1)  +  (1 + 1)  +  6
//Total of 17 Points

void ProgrammingSkills2(void)
{
    //Score left top flag - 2pts
    drive(-3, 15);
    fire();

    //Get ball from under cap and flip cap - 3pts
    turn(90, 30);
    intake(100);
    drive(-33, 50);
    vex::sleepMs(500);
    intake(30);
    drive(4, 20);
    turn(-190, 30);
    drive(7, 20);

    Flipper1.rotateTo(270, vex::rotationUnits::deg, 70, vex::velocityUnits::pct, false);
    Flipper2.rotateTo(270, vex::rotationUnits::deg, 70, vex::velocityUnits::pct);
    Flipper1.rotateTo(0, vex::rotationUnits::deg, 100, vex::velocityUnits::pct, false);
    Flipper2.rotateTo(0, vex::rotationUnits::deg, 100, vex::velocityUnits::pct);

    //Align against wall
    intake(-10);
    drive(-40, 50);
    intake(0);
    rawmove(800, -40);

    //Ram left bottom flag and align - 4pts
    drive(8, 20);
    intake(-10);
    turn(86, 15);
    intake(10);
    rawmove(1500, 90);
    rawmove(750, 60);
    drive(-4, 20);
    turn(-10, 15);
    rawmove(1000, 40);

    //Back up and shoot left middle flag - 6pts
    intake(10);
    drive(-19, 25);
    turn(-6, 10);
    vex::sleepMs(200);
    fire();
    intake(-30);

    //swing turn to flip center bottom flag - 7pts
    turn(-62, 40);
    clear();
    FL.rotateTo(480, vex::rotationUnits::deg, -60, vex::velocityUnits::pct, false);
    BL.rotateTo(480, vex::rotationUnits::deg, -60, vex::velocityUnits::pct, false);
    FR.rotateTo(360, vex::rotationUnits::deg, -45, vex::velocityUnits::pct, false);
    BR.rotateTo(360, vex::rotationUnits::deg, -45, vex::velocityUnits::pct, true);

    drive(34, 40);
    turn(90, 25);
    rawmove(1000, 60);

    //Back up and turn right to flip cap - 8pts
    drive(-26, 30);
    turn(-90, 30);
    drive(47, 40);
    turn(-10, 15);
    flip();

    //Ram against right bottom flag - 9pts
    turn(95, 25);
    rawmove(1500, 70);

    //Back up and get ball from under cap
    drive(-50, 40);
    turn(-85, 30);
    rawmove(1750, 60);
    intake(100);
    drive(-43, 50);
    vex::sleepMs(500);
    intake(30);
    drive(10, 20);

    //Align against platform to turn and shoot right top flag - 11pts
    turn(90, 10);
    intake(0);
    rawmove(1000, -40);
    drive(4, 10);
    clear();
    FL.rotateTo(80, vex::rotationUnits::deg, -15, vex::velocityUnits::pct, false);
    BL.rotateTo(80, vex::rotationUnits::deg, -15, vex::velocityUnits::pct, true);
    fire();

    //Center Park - 17pts
    intake(30);
    rawmove(1000, -40);
    drive(-26, 90);
    intake(100);
    vex::sleepMs(400);
    turn(-105, 15);
    intake(30);
    vex::sleepMs(300);
    rawmove(750, -30);
    drive(-27, 90);
    intake(0);
}