// VEX V5 C++ Project with Competition Template
#include "vex.h"
#include "robot_config OS1.h"
#include "LCD_Select.h"
#include "Voids2.h"
#include "AutonCollaboration.h"
#include "DriverControl.h"
#include "DriverLCD.h"
#include "ProgrammingSkills5.h"
#include "Autonomous2.h"

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                             Competition Tasks                             */
/*                                                                           */
/*---------------------------------------------------------------------------*/

/*/////////////////////////////////*/
/*       Pre-Autonomous Task       */
/*/////////////////////////////////*/
void pre_auton(void)
{
    Remote.Screen.print(" 1 3 2 0 D ");

    FL.setStopping(vex::brakeType::coast);
    BL.setStopping(vex::brakeType::coast);
    FR.setStopping(vex::brakeType::coast);
    BR.setStopping(vex::brakeType::coast);
    FR.setReversed(true);
    BR.setReversed(true);

    Rollers.setStopping(vex::brakeType::coast);
    Shooter.setStopping(vex::brakeType::hold);
    Flipper1.setStopping(vex::brakeType::coast);
    Flipper2.setStopping(vex::brakeType::coast);
    Flipper1.setReversed(true);

    vex::thread select(AutonSelect);
}

/*/////////////////////////////*/
/*       Autonomous Task       */
/*/////////////////////////////*/
void autonomous(void)
{

    thread autonshoot(Catapult_Auton);

    //autonomousSelection = 3;
    //ParkChoice = 1;
    //Autonomous();

    lineUp(200, 100);

    //CD_red();

    //ProgrammingSkills5();

    intake(0);
}

/*/////////////////////////////////*/
/*       Driver Control Task       */
/*/////////////////////////////////*/
void usercontrol(void)
{

    FL.setStopping(vex::brakeType::coast);
    BL.setStopping(vex::brakeType::coast);
    FR.setStopping(vex::brakeType::coast);
    BR.setStopping(vex::brakeType::coast);

    Flipper1.setStopping(vex::brakeType::coast);
    Flipper2.setStopping(vex::brakeType::coast);

    FL.setMaxTorque(100, percentUnits::pct);
    BL.setMaxTorque(100, percentUnits::pct);
    FR.setMaxTorque(100, percentUnits::pct);
    BR.setMaxTorque(100, percentUnits::pct);

    Flipper1.setMaxTorque(100, percentUnits::pct);
    Flipper2.setMaxTorque(100, percentUnits::pct);

    Brain.Screen.clearScreen();

    vex::thread rainbow(DriverLCD);
    vex::thread toggles(ToggleThread);
    vex::thread flipping(FlipperTask);

    while (Competition.isDriverControl() && Competition.isEnabled())
    {

        BaseTask();
        //SplitStick();
        IntakeTask();
        CatapultTask();

        vex::sleepMs(10); //Sleep the task for a short amount of time to prevent wasted resources.
    }
}

/*///////////////////////*/
/*       Main Task       */
/*///////////////////////*/
int main()
{
    //Set up callbacks for autonomous and driver control periods.
    Competition.autonomous(autonomous);
    Competition.drivercontrol(usercontrol);

    //Run the pre-autonomous function.
    pre_auton();
}
