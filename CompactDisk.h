void CD_red(void)
{

    vex::sleepMs(500);
    fire();
    turn(100, 50);
    rawmove(1000, 70);
    intake(100);
    drive(-43, 80);
    vex::sleepMs(300);
    intake(30);

    clear();
    // FL.rotateTo(310, vex::rotationUnits::deg, -15, vex::velocityUnits::pct, false);
    // BL.rotateTo(310, vex::rotationUnits::deg, -15, vex::velocityUnits::pct, false);
    // FR.rotateTo(1240, vex::rotationUnits::deg, -60, vex::velocityUnits::pct, false);
    // BR.rotateTo(1240, vex::rotationUnits::deg, -60, vex::velocityUnits::pct, true);

    FL.rotateTo(460, vex::rotationUnits::deg, -20, vex::velocityUnits::pct, false);
    BL.rotateTo(460, vex::rotationUnits::deg, -20, vex::velocityUnits::pct, false);
    FR.rotateTo(1380, vex::rotationUnits::deg, -60, vex::velocityUnits::pct, false);
    BR.rotateTo(1380, vex::rotationUnits::deg, -60, vex::velocityUnits::pct, true);

    intake(10);

    drive(14, 70);
    flip();
    turn(95, 60);
    rawmove(700, -70);
    rawmove(1500, 60);
    vex::sleepMs(100);

    clear();

    FR.rotateTo(-60, vex::rotationUnits::deg, 15, vex::velocityUnits::pct, false);
    BR.rotateTo(-60, vex::rotationUnits::deg, 15, vex::velocityUnits::pct, true);

    fire();
    intake(0);

    turn(-190, 60);
    rawmove(750, -40);
    drive(-24, 100);
}

/*void CD_blue(void)
{
    fire();
    turn(-120, 50);
    rawmove(1000, 70);
    intake(100);
    drive(-42, 80);
    vex::sleepMs(300);
    drive(10, 60);
    intake(10);

    clear();
    FR.rotateTo(230, vex::rotationUnits::deg, -10, vex::velocityUnits::pct, false);
    BR.rotateTo(230, vex::rotationUnits::deg, -10, vex::velocityUnits::pct, false);
    FL.rotateTo(1150, vex::rotationUnits::deg, -50, vex::velocityUnits::pct, false);
    BL.rotateTo(1150, vex::rotationUnits::deg, -50, vex::velocityUnits::pct, true);

    drive(20, 70);
    flip();
    //drive(-5, 40);
    turn(-105, 60);
    rawmove(600, -70);
    rawmove(1500, 60);
    vex::sleepMs(100);

    clear();

    FL.rotateTo(-60, vex::rotationUnits::deg, 15, vex::velocityUnits::pct, false);
    BL.rotateTo(-60, vex::rotationUnits::deg, 15, vex::velocityUnits::pct, true);

    fire();

    turn(175, 60);
    rawmove(750, -40);
    drive(-22, 100);
}*/