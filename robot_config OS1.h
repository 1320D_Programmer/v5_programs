using namespace vex;

//Creates a competition object that allows access to Competition methods.
vex::competition Competition;

//#region config_globals

//Define the V5 Brain
vex::brain Brain;

//Define the Remote Control
vex::controller Remote = vex::controller();

//Define the four base motors at a High Speed 18 to 1 internal gear ratio
vex::motor FR = vex::motor(vex::PORT12, vex::gearSetting::ratio18_1);
vex::motor BR = vex::motor(vex::PORT5, vex::gearSetting::ratio18_1);
vex::motor FL = vex::motor(vex::PORT4, vex::gearSetting::ratio18_1);
vex::motor BL = vex::motor(vex::PORT2, vex::gearSetting::ratio18_1);

//Define Catapult motor at a Torque 36 to 1 internal gear ratio
vex::motor Shooter = vex::motor(vex::PORT1, vex::gearSetting::ratio36_1);

//Define Intake motor at a Turbo 6 to 1 internal gear ratio
vex::motor Rollers = vex::motor(vex::PORT6, vex::gearSetting::ratio6_1);

//Define Cap Flipper motor at a Torque 36 to 1 internal gear ratio
vex::motor Flipper1 = vex::motor(vex::PORT7, vex::gearSetting::ratio18_1);
vex::motor Flipper2 = vex::motor(vex::PORT9, vex::gearSetting::ratio18_1);

//Define the Catapult limit switch
vex::limit CatLimit = vex::limit(Brain.ThreeWirePort.H);

//Define the two Ultrasonic Sensors
vex::sonar LeftSonar = vex::sonar(Brain.ThreeWirePort.A);
vex::sonar RightSonar = vex::sonar(Brain.ThreeWirePort.B);

vex::encoder LeftQuad = vex::encoder(Brain.ThreeWirePort.C);
vex::encoder RightQuad = vex::encoder(Brain.ThreeWirePort.E);

//Define the Vision Sensor
vex::vision VisionSensor(vex::PORT9);

//Define the Vision Sensor color signatures
vex::vision::signature sig_BLUE_FLAG(1, -2935, -2419, -2677, 6837, 7813, 7325, 11, 0);
vex::vision::signature sig_RED_FLAG(2, 4983, 5863, 5423, -1075, -175, -625, 5.4, 0);
vex::vision::signature sig_GREEN_EDGE(3, -3353, -2959, -3156, -4817, -4269, -4543, 5.5, 0);

//#endregion config_globals

bool AutoCatapult = false; //Enable the automatic Catapult resetting
bool AutoIntake = true;    //Disable the Intake while the Catapult is not in position
bool Hold = false;         //Hold the Cap Flipper Horizontally
bool Height = false;       //
bool shoot = false;        //Fire the Catapult while in Autonomous
bool falseprophet = false; //
bool ParkingBrake = false; //
int time = 0;              //Variable for timers used in various functions
int rollerpower;
float inchConvert = 28.65;   //Conversion factor from inches to encoder ticks (Calculated value is 28.648)
float degreesConvert = 2.57; //Conversion factor from turning degrees to encoder ticks (Calculated value is 2.625)