// VEX V5 C++ Project with Competition Template
#include "vex.h"
#include "robot_config OS1.h"
#include "LCD_Select.h"
#include "Voids2.h"
//#include "Vision.h"
#include "DriverControl.h"
#include "DriverLCD.h"
#include "ProgrammingSkills5.h"
#include "Autonomous2.h"

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                             Competition Tasks                             */
/*                                                                           */
/*---------------------------------------------------------------------------*/

void motorTemp(void)
{
    Brain.Screen.setFont(vex::fontType::mono20);

    while (true)
    {
        Brain.Screen.render();

        Brain.Screen.setFillColor(vex::color::transparent);
        Brain.Screen.setPenColor(vex::color::white);

        Brain.Screen.printAt(10, 25, "FL: %1.0f Unsnazzyness", FL.temperature(vex::percentUnits::pct));
        Brain.Screen.printAt(10, 50, "BL: %1.0f Unsnazzyness", BL.temperature(vex::percentUnits::pct));
        Brain.Screen.printAt(10, 75, "FR: %1.0f Unsnazzyness", FR.temperature(vex::percentUnits::pct));
        Brain.Screen.printAt(10, 100, "BR: %1.0f Unsnazzyness", BR.temperature(vex::percentUnits::pct));

        Brain.Screen.printAt(10, 125, "Catapult: %1.0f Unsnazzyness", Shooter.temperature(vex::percentUnits::pct));
        Brain.Screen.printAt(10, 150, "Intake: %1.0f Unsnazzyness", Rollers.temperature(vex::percentUnits::pct));
        Brain.Screen.printAt(10, 175, "Flipper1: %1.0f Unsnazzyness", Flipper1.temperature(vex::percentUnits::pct));
        Brain.Screen.printAt(10, 200, "Flipper2: %1.0f Unsnazzyness", Flipper2.temperature(vex::percentUnits::pct));

        Brain.Screen.setFillColor(vex::color::black);
        Brain.Screen.setPenColor(vex::color::black);

        Brain.Screen.drawRectangle(265, 0, 480, 280);

        Brain.Screen.drawRectangle(205, 0, 480, 110);
    }
}

/*/////////////////////////////////*/
/*       Pre-Autonomous Task       */
/*/////////////////////////////////*/
void pre_auton(void)
{
    Remote.Screen.print(" 1 3 2 0 D ");

    FL.setStopping(vex::brakeType::coast);
    BL.setStopping(vex::brakeType::coast);
    FR.setStopping(vex::brakeType::coast);
    BR.setStopping(vex::brakeType::coast);
    FR.setReversed(true);
    BR.setReversed(true);

    Rollers.setStopping(vex::brakeType::coast);
    Shooter.setStopping(vex::brakeType::hold);
    Flipper1.setStopping(vex::brakeType::coast);
    Flipper2.setStopping(vex::brakeType::coast);
}

/*/////////////////////////////*/
/*       Autonomous Task       */
/*/////////////////////////////*/
void autonomous(void)
{
}

/*/////////////////////////////////*/
/*       Driver Control Task       */
/*/////////////////////////////////*/
void usercontrol(void)
{

    FL.setStopping(vex::brakeType::coast);
    BL.setStopping(vex::brakeType::coast);
    FR.setStopping(vex::brakeType::coast);
    BR.setStopping(vex::brakeType::coast);

    Flipper1.setStopping(vex::brakeType::coast);
    Flipper2.setStopping(vex::brakeType::coast);

    FL.setMaxTorque(100, percentUnits::pct);
    BL.setMaxTorque(100, percentUnits::pct);
    FR.setMaxTorque(100, percentUnits::pct);
    BR.setMaxTorque(100, percentUnits::pct);

    Flipper1.setMaxTorque(100, percentUnits::pct);
    Flipper2.setMaxTorque(100, percentUnits::pct);

    Brain.Screen.clearScreen();

    vex::thread toggles(ToggleThread);
    vex::thread flipping(FlipperTask);
    vex::thread temps(motorTemp);

    vex::sleepMs(350);

    AutoCatapult = false;

    while (Competition.isDriverControl() && Competition.isEnabled())
    {

        BaseTask();
        IntakeTask();
        CatapultTask();

        vex::sleepMs(10); //Sleep the task for a short amount of time to prevent wasted resources.
    }
}

/*///////////////////////*/
/*       Main Task       */
/*///////////////////////*/
int main()
{
    //Set up callbacks for autonomous and driver control periods.
    Competition.autonomous(autonomous);
    Competition.drivercontrol(usercontrol);

    //Run the pre-autonomous function.
    pre_auton();
}
