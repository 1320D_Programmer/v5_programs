/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                           Autonomous Functions                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/

void clear(void)
{
    FL.resetRotation();
    BL.resetRotation();
    FR.resetRotation();
    BR.resetRotation();
}

void SetTorque(int maxTorque)
{
    FL.setMaxTorque(maxTorque, percentUnits::pct);
    BL.setMaxTorque(maxTorque, percentUnits::pct);
    FR.setMaxTorque(maxTorque, percentUnits::pct);
    BR.setMaxTorque(maxTorque, percentUnits::pct);
}

void setBrake(int stop)
{
    if (stop == 0)
    {
        FL.setStopping(vex::brakeType::coast);
        BL.setStopping(vex::brakeType::coast);
        FR.setStopping(vex::brakeType::coast);
        BR.setStopping(vex::brakeType::coast);
    }
    else if (stop == 1)
    {
        FL.setStopping(vex::brakeType::brake);
        BL.setStopping(vex::brakeType::brake);
        FR.setStopping(vex::brakeType::brake);
        BR.setStopping(vex::brakeType::brake);
    }
    else if (stop == 2)
    {
        FL.setStopping(vex::brakeType::hold);
        BL.setStopping(vex::brakeType::hold);
        FR.setStopping(vex::brakeType::hold);
        BR.setStopping(vex::brakeType::hold);
    }
}

void move(int leftpower, int rightpower)
{
    FL.spin(vex::directionType::fwd, leftpower, vex::percentUnits::pct);
    BL.spin(vex::directionType::fwd, leftpower, vex::percentUnits::pct);
    FR.spin(vex::directionType::fwd, rightpower, vex::percentUnits::pct);
    BR.spin(vex::directionType::fwd, rightpower, vex::percentUnits::pct);
}

void cease(int stop)
{
    if (stop == 0)
    {
        FL.stop(brakeType::coast);
        BL.stop(brakeType::coast);
        FR.stop(brakeType::coast);
        BR.stop(brakeType::coast);
    }
    else if (stop == 1)
    {
        FL.stop(brakeType::brake);
        BL.stop(brakeType::brake);
        FR.stop(brakeType::brake);
        BR.stop(brakeType::brake);
    }
    else if (stop == 2)
    {
        FL.stop(brakeType::hold);
        BL.stop(brakeType::hold);
        FR.stop(brakeType::hold);
        BR.stop(brakeType::hold);
    }
}

void drive(double distance, int velocity)
{
    clear();

    int increment = velocity / 20;
    int currentSpeed = increment;
    double target = distance * inchConvert;
    if (distance > 0)
    {
        if (distance > 6)
        {
            for (int rampUp = 0; rampUp < 16; rampUp++)
            {
                currentSpeed += increment;
                FL.spin(vex::directionType::rev, -currentSpeed, vex::percentUnits::pct);
                BL.spin(vex::directionType::rev, -currentSpeed, vex::percentUnits::pct);
                FR.spin(vex::directionType::rev, -currentSpeed, vex::percentUnits::pct);
                BR.spin(vex::directionType::rev, -currentSpeed, vex::percentUnits::pct);
                vex::sleepMs(10);
            }
        }

        FL.rotateTo(target, vex::rotationUnits::deg, -velocity, vex::velocityUnits::pct, false);
        BL.rotateTo(target, vex::rotationUnits::deg, -velocity, vex::velocityUnits::pct, false);
        FR.rotateTo(target, vex::rotationUnits::deg, -velocity, vex::velocityUnits::pct, false);
        BR.rotateTo(target, vex::rotationUnits::deg, -velocity, vex::velocityUnits::pct, true);
    }
    else
    {

        if (distance < -6)
        {
            for (int rampUp = 0; rampUp < 16; rampUp++)
            {
                currentSpeed += increment;
                FL.spin(vex::directionType::rev, currentSpeed, vex::percentUnits::pct);
                BL.spin(vex::directionType::rev, currentSpeed, vex::percentUnits::pct);
                FR.spin(vex::directionType::rev, currentSpeed, vex::percentUnits::pct);
                BR.spin(vex::directionType::rev, currentSpeed, vex::percentUnits::pct);
                vex::sleepMs(10);
            }
        }

        FL.rotateTo(target, vex::rotationUnits::deg, velocity, vex::velocityUnits::pct, false);
        BL.rotateTo(target, vex::rotationUnits::deg, velocity, vex::velocityUnits::pct, false);
        FR.rotateTo(target, vex::rotationUnits::deg, velocity, vex::velocityUnits::pct, false);
        BR.rotateTo(target, vex::rotationUnits::deg, velocity, vex::velocityUnits::pct, true);
    }
}

void rawmove(int time, int velocity)
{
    FL.setMaxTorque(10, percentUnits::pct);
    BL.setMaxTorque(10, percentUnits::pct);
    FR.setMaxTorque(10, percentUnits::pct);
    BR.setMaxTorque(10, percentUnits::pct);

    int startTime = Brain.timer(timeUnits::msec);

    while (Brain.timer(timeUnits::msec) < startTime + time)
    {
        FL.spin(vex::directionType::fwd, velocity, vex::percentUnits::pct);
        BL.spin(vex::directionType::fwd, velocity, vex::percentUnits::pct);
        FR.spin(vex::directionType::fwd, velocity, vex::percentUnits::pct);
        BR.spin(vex::directionType::fwd, velocity, vex::percentUnits::pct);
    }
    FL.stop(brakeType::coast);
    BL.stop(brakeType::coast);
    FR.stop(brakeType::coast);
    BR.stop(brakeType::coast);

    FL.setMaxTorque(100, percentUnits::pct);
    BL.setMaxTorque(100, percentUnits::pct);
    FR.setMaxTorque(100, percentUnits::pct);
    BR.setMaxTorque(100, percentUnits::pct);
}

void turn(float rotation, int velocity)
{
    clear();

    float target = rotation * degreesConvert;

    FL.rotateTo(-target, vex::rotationUnits::deg, velocity, vex::velocityUnits::pct, false);
    BL.rotateTo(-target, vex::rotationUnits::deg, velocity, vex::velocityUnits::pct, false);
    FR.rotateTo(target, vex::rotationUnits::deg, velocity, vex::velocityUnits::pct, false);
    BR.rotateTo(target, vex::rotationUnits::deg, velocity, vex::velocityUnits::pct, true);
}

void flip(void)
{
    Flipper1.startRotateTo(0, vex::rotationUnits::deg, 100, vex::velocityUnits::pct);
    Flipper2.startRotateTo(0, vex::rotationUnits::deg, 100, vex::velocityUnits::pct);
    time = 0;
    while (Flipper1.rotation(vex::rotationUnits::deg) > 10 && time < 2000)
    {
        vex::sleepMs(10);
        time += 10;
    }
    Flipper1.rotateTo(220, vex::rotationUnits::deg, 80, vex::velocityUnits::pct, false);
    Flipper2.rotateTo(220, vex::rotationUnits::deg, 80, vex::velocityUnits::pct);
    Flipper1.setMaxTorque(20, percentUnits::pct);
    Flipper2.setMaxTorque(20, percentUnits::pct);
    Flipper1.startRotateTo(0, vex::rotationUnits::deg, 100, vex::velocityUnits::pct);
    Flipper2.startRotateTo(0, vex::rotationUnits::deg, 100, vex::velocityUnits::pct);
    time = 0;
    while (Flipper1.rotation(vex::rotationUnits::deg) > 10 && time < 2000)
    {
        vex::sleepMs(10);
        time += 10;
    }
    Flipper1.setMaxTorque(100, percentUnits::pct);
    Flipper2.setMaxTorque(100, percentUnits::pct);
}

void intake(int velocity)
{
    Rollers.spin(vex::directionType::rev, velocity, vex::velocityUnits::pct);
}

void Catapult_Auton(void)
{
    while (Competition.isAutonomous() && Competition.isEnabled())
    {
        if (shoot == false)
        {
            if (CatLimit.pressing() == 0)
            {
                Shooter.spin(vex::directionType::fwd, 100, vex::velocityUnits::pct);
            }
            else
            {
                Shooter.stop();
            }
        }
        else
        {
            Shooter.spin(vex::directionType::fwd, 100, vex::velocityUnits::pct);

            if (CatLimit.pressing() == 0)
            {
                while (CatLimit.pressing() == 0)
                {
                    vex::sleepMs(10);
                }
            }
            vex::sleepMs(300);
            shoot = false;
        }
        vex::sleepMs(10); //Sleep the task for a short amount of time to prevent wasted resources.
    }
}

void fire(void)
{
    if (CatLimit.pressing() == 0)
    {
        while (CatLimit.pressing() == 0)
        {
            vex::sleepMs(10);
        }
        vex::sleepMs(100);
    }
    shoot = true;
    while (shoot == true)
    {
        vex::sleepMs(10);
    }
    vex::sleepMs(100);
}

void AutonLCD(void)
{
    Brain.Screen.clearScreen();

    Brain.Screen.setPenWidth(1);
    Brain.Screen.setFont(vex::fontType::mono30);

    switch (autonomousSelection)
    {
    //Red Front Flags
    case 0:
        Brain.Screen.setFillColor(vex::color(0x400000));
        Brain.Screen.setPenColor(vex::color(0x400000));
        Brain.Screen.drawRectangle(0, 0, 480, 280);
        Brain.Screen.setPenColor(vex::color::white);
        Brain.Screen.printAt(10, 160, "  Red Front Flags is running");

        break;
    //Red Back Caps
    case 1:
        Brain.Screen.setFillColor(vex::color(0x400000));
        Brain.Screen.setPenColor(vex::color(0x400000));
        Brain.Screen.drawRectangle(0, 0, 480, 280);
        Brain.Screen.setPenColor(vex::color::white);
        Brain.Screen.printAt(10, 160, "   Red Back Caps is running");
        break;
    //Programming Skills
    case 2:
        Brain.Screen.setFillColor(vex::color(0x400000));
        Brain.Screen.setPenColor(vex::color(0x400000));
        Brain.Screen.drawRectangle(0, 0, 480, 280);
        Brain.Screen.setPenColor(vex::color::white);
        Brain.Screen.printAt(10, 160, " Programming Skills is running");
        break;
    //Blue Front Flags
    case 3:
        Brain.Screen.setFillColor(vex::color(0x000040));
        Brain.Screen.setPenColor(vex::color(0x000040));
        Brain.Screen.drawRectangle(0, 0, 480, 280);
        Brain.Screen.setPenColor(vex::color::white);
        Brain.Screen.printAt(10, 160, "  Blue Front Flags is running");

        break;
    //Blue Back Caps
    case 4:
        Brain.Screen.setFillColor(vex::color(0x000040));
        Brain.Screen.setPenColor(vex::color(0x000040));
        Brain.Screen.drawRectangle(0, 0, 480, 280);
        Brain.Screen.setPenColor(vex::color::white);
        Brain.Screen.printAt(10, 160, "   Blue Back Caps Flags is running");
        break;
    //Do Nothing
    default:
        Brain.Screen.printAt(10, 160, "You didn't select an Autonomous");
        Brain.Screen.printAt(10, 210, "        Sucks to suck        ");
        break;
    }

    Brain.Screen.setPenColor(vex::color::white);
    Brain.Screen.setFillColor(vex::color::red);
    if (falseprophet)
    {
        Brain.Screen.setFont(vex::fontType::prop40);

        Brain.Screen.printAt(44, 75, " F A L S E  P R O P H E T ");

        Brain.Screen.setFillColor(vex::color::transparent);
        Brain.Screen.setPenWidth(2);

        Brain.Screen.drawRectangle(43, 36, 390, 61);
    }
    else
    {
        Brain.Screen.setFont(vex::fontType::prop60);

        Brain.Screen.printAt(10, 80, " S O L I D A R I T Y ");

        Brain.Screen.setFillColor(vex::color::transparent);
        Brain.Screen.setPenWidth(2);

        Brain.Screen.drawRectangle(9, 23, 460, 90);
    }
}
